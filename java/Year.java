class Year extends Day {
    public Year(int year) {
        super(year);
        this._set_value(year);
    }

    public void print() {
        System.out.println("Year: " + this.value);
    }

    protected void _set_value(int value) {
        if (value >= 1900) {
            this.value = value;
        } else {
            this.value = 1900;
        }
    }

}