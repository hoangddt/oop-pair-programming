public class Day {
    protected int value;
    protected int max;
    protected int min;

    public static void main (String[] args) {
        Day d = new Day(30);
        d.print();
    }

    public Day(int day) {
        this.max = 31;
        this.min = 1;
        this._set_value(day);
    }

    protected void _set_value(int value) {
        if (value > this.max) {
            this.value = this.min;
        }
        else if (value < this.min) {
            this.value = this.max;
        }
        else {
            this.value = value;
        }
    }

    public void increase() {
        this._set_value(this.value + 1);
    }

    public void decrease() {
        this._set_value(this.value - 1);
    }

    public void print() {
        System.out.println("Day: " + this.value);
    }
}