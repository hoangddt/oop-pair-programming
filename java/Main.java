import java.util.*;
import java.lang.*;
import java.io.*;

class Main
{
    public static void main (String[] args)
    {
        Main instance = new Main();
        instance.polymorphism();
    }

    public void normalCreattion() {
        Day d = new Day(15);
        d.increase();
        d.print();

        Month m = new Month(10);
        m.increase();
        m.increase();
        m.increase();
        m.print();

        Year y = new Year(15);
        y.increase();
        y.print();
    }

    public void polymorphism() {
        Day[] arr = {new Day(10), new Month(9), new Year(1995), new Year(4500)};

        for (int i = 0; i < arr.length; i++) {
            arr[i].increase();
            arr[i].print();
        }
    }
}