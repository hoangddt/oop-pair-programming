class Month extends Day {
    public Month(int month) {
        super(month);
        this.max = 12;
        this.min = 1;
        this._set_value(month);
    }

    public void print() {
        System.out.println("Month: " + this.value);
    }
}