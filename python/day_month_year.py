class Day():
    """represent day from 1 to 31"""
    def __init__(self, day):
        self.min = 1
        self.max = 31
        self.set_value(day)

    def increase(self):
        self.set_value(self.value + 1)

    def decrease(self):
        self.set_value(self.value - 1)  

    def set_value(self, value):
        if value > self.max:
            self.value = self.min
        elif value < self.min:
            self.value = self.max
        else:
            self.value = value

    def print(self):
        print("Day: %s" % (self.value))


class Month(Day):
    def __init__(self, month):
        super(Month, self).__init__(month)
        self.min = 1
        self.max = 12
        self.set_value(month)

    def print(self):
        print("Month: %s" % (self.value))


class Year(Day):
    def __init__(self, year):
        super(Year, self).__init__(year)
        self.set_value(year)

    def set_value(self, value):
        if value // 1000 > 0:
            self.value = value
        else:
            self.value = 1900
    
    def print(self):
        print("Year: %s" % (self.value))

if __name__ == '__main__':
    d = Day(32)
    d.print()
    m = Month(10)
    m.increase()
    m.increase()
    m.increase()
    m.print()

    y = Year(0)
    y.print()
    y.increase()
    y.print()